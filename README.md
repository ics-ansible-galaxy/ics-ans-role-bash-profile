# ics-ans-role-bash-profile

Ansible role to configure the systemwide bash profile.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-bash-profile
```

## License

BSD 2-clause
